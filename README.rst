``docopt.scala`` is a JVM implementation of the docopt
======================================================

USAGE
=====
- For a scala example, see ``src/test/scala/org/docopt/Testee.scala`` and
  ``src/test/scripts/testee``
- For a java example, see ``src/test/java/org/docopt/NavalFate.java`` and
  ``src/test/scripts/naval_fate``

COVERAGE
========
The current coverage is 150/164 language agnostic tests.
